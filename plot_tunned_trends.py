import glob
import json
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn

sn.set(font_scale = 1)
sn.set_style("darkgrid")
folder_path = '/Users/nirandikawanigasekara/PycharmProjects/ApproxZooming/plots/tuned_results/2/'


def plot_heat_map(_df_last_ctrs, algo_name, v_type, cbar_f, ylabel, noise):
    plt.figure(figsize=(4, 4), dpi=300)

    ax = sn.heatmap(_df_last_ctrs, cbar=cbar_f, vmin=0.7, vmax=1)

    print(algo_name)
    print(_df_last_ctrs.head(10))

    if not ylabel:
        ax.yaxis.label.set_visible(False)
    # plt.legend(prop={'size': 8})
    plt.title(r'sigma'+' ='+str(noise))
    plt.savefig(folder_path + algo_name + "_" + v_type + ".pdf")
    plt.close()


folders = [name for name in os.listdir(folder_path) if os.path.isdir(folder_path + name)]

print(folders)

cols = ['trials', 'Approx-Zooming', 'Approx-Zooming-No-Arm-Similarity',
        'Approx-Zooming-Similarity-Metric', 'Approx-Zooming-True']
df_last_ctrs = pd.DataFrame(columns=cols)

for folder in folders:
    ctr_file_name = glob.glob(folder_path + folder + '/ctr_[0-9]*.csv')
    assert len(ctr_file_name) == 1
    df_ctr = pd.read_csv(ctr_file_name[0])  #
    df_ctr.columns = cols
    df_tmp = df_ctr.tail(1).reset_index()

    print(df_tmp['Approx-Zooming-True'])

    assert df_tmp.shape[0] == 1
    other_details_path = folder_path + folder + '/Approx-Zooming.json'
    with open(other_details_path, 'r') as f:
        detail_obj = json.load(f)
    f.close()
    df_tmp.loc[0, 'trials'] = detail_obj["T"]
    df_tmp['arms'] = [detail_obj["no_A"]]
    df_tmp['noise'] = [detail_obj["noise"]]
    df_last_ctrs = df_last_ctrs.append(df_tmp, sort=False)

for algo in cols[1:]:
    # for a in [50, 100, 200]:
    #     df_last_ctrs_trials_vs_noise = df_last_ctrs[df_last_ctrs['arms'] == a].pivot(index='noise', columns='trials',
    #                                                                                  values=algo)
    #     cbar_flag = False
    #     label = False
    #     if a == 200:
    #         cbar_flag = True
    #     elif a == 50:
    #         label = True
    #
    #     plot_heat_map(df_last_ctrs_trials_vs_noise, algo, "trials_vs_noise_" + str(a), cbar_flag, label)

    for n in [1e-1, 1e-2, 1e-3]:
        cbar_flag = False
        label = False
        if n == 1e-3:
            cbar_flag = True
        elif n == 1e-1:
            label = True
        df_last_ctrs_trials_vs_arms = df_last_ctrs[df_last_ctrs['noise'] == n].pivot(index='arms', columns='trials',
                                                                                     values=algo)
        plot_heat_map(df_last_ctrs_trials_vs_arms, algo, "trials_vs_arms_" + str(int(n * 1000)), cbar_flag, label, n)
