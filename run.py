import configurations as con
import random
import sim_estimated_dist as sed
import sim_true_distance as std
import sim_metric as sm
import sim_heuristic as sh
import seaborn as sn
from scipy.interpolate import spline
import context_partition_node as cpn
import numpy as np
from scipy.stats import binned_statistic
import matplotlib.pyplot as plt
import sim_single as snd
import datetime
import os
import pandas as pd
import json
import sim_gp_single_arm as gp_single


plt.switch_backend('agg')
sn.set_style("darkgrid")



def get_random_context():
    return random.uniform(0, 1)


def get_avg_cumm_reward(_cum_r_list):
    if len(_cum_r_list)>0:
        cumm_r_plot = [_cum_r_list[0]]
        cumm_r = _cum_r_list[0]
        for idx, cr in enumerate(_cum_r_list[1:]):
            cumm_r_plot.append((cumm_r + cr) / (idx + 2.0))
            cumm_r = cumm_r + cr
    else:
        return []

    bin_means = binned_statistic(range(0,len(cumm_r_plot)), cumm_r_plot, statistic='mean', bins=1000)[0]
    return bin_means.tolist()


def plot_arm_frequency(inst_algo, file_name, flag):
    plt.clf()
    fig = plt.figure(figsize=(5, 1.7), dpi=300)
    f = inst_algo.arm_frequency
    #print("Frequency", f)
    e = TRIALS/4
    step = TRIALS/4
    col = 1
    cbar_ax = fig.add_axes([0.6, .4, .3, .03])
    for s in range(0, len(f), step):
        heatmap_data_all = np.zeros((NO_ARMS, 10))
        heatmap_data_explore = np.zeros((NO_ARMS,10))
        heatmap_data_exploit = np.zeros((NO_ARMS,10))
        for _tuple in f[s:e]:
            a = _tuple[1]
            cont = int(_tuple[0]/0.1)
            heatmap_data_all[(a), cont] = heatmap_data_all[a, cont] + 1
            if _tuple[2] == 1:
                heatmap_data_exploit[(a), cont] = heatmap_data_exploit[a, cont] + 1
            if _tuple[2] == 0:
                heatmap_data_explore[(a), cont] = heatmap_data_explore[a, cont] + 1
        e = min(e+step, len(f))
        # print(heatmap_data)
        sb = plt.subplot(1,4,col)
        if flag == 2:
            sn.heatmap(heatmap_data_all, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"})
        elif flag == 1:
            sn.heatmap(heatmap_data_exploit, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"})
        elif flag == 0:
            sn.heatmap(heatmap_data_explore, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"})
        sb.set_xlabel("t = "+str(col*25000))
        col = col + 1
    fig.text(0.5, 0.0, 'Context Interval', ha='center')
    fig.text(0.0, 0.5, 'Finite Arms', va='center', rotation='vertical')
    # fig.suptitle('Selected Arm Type Frequency_'+str(flag))
    plt.tight_layout()
    plt.savefig(pngs_dir_name+"arm_frequency_"+file_name+"_"+str(flag)+".pdf")

    # if len(f) > 0:
    #     histogram_data_arms = list(zip(*f)[1])
    #     histogram_data_context = list(zip(*f)[0])
    #     plt.clf()
    #     ax = plt.subplot(2,1,1)
    #     sn.distplot(histogram_data_arms, kde=False, rug=True)
    #     ax.set_title("arm selection distribution")
    #     ax =plt.subplot(2,1,2)
    #     sn.distplot(histogram_data_context, kde=False, rug=True)
    #     ax.set_title("context distribution")
    #     plt.savefig(pngs_dir_name+"total_arm_f_"+file_name+".pdf")



def plot_graphs(algos):

    dict_cumm_rewards = {}

    for algo_name in algos.keys():
        plot_arm_frequency(algos[algo_name], algo_name,2)
        if 'our-method' in algo_name:
            plot_arm_frequency(algos[algo_name], algo_name,1)
            plot_arm_frequency(algos[algo_name], algo_name,0)
        dict_cumm_rewards[algo_name] = get_avg_cumm_reward(algos[algo_name].cum_r_list)


    plot_ctr(dict_cumm_rewards)

    # if 'single' in algos.keys():
    #     for a in algos["single"].dict_arms.values():
    #         plt.clf()
    #         a.plot()
    #
    #
    #     df = pd.DataFrame({'get_reward': algos["single"].get_reward_elapsed_time,
    #                        'update': algos["single"].update_elapsed_time})
    #
    #     sn.lineplot(data=df)
    #     plt.xlabel("trial")
    #     plt.ylabel("time")
    #     plt.savefig(pngs_dir_name+"elapsed_time_for_GP_single.pdf")

    if 'our-method' in algos.keys():
        plt.clf()
        df_our_method_explore_exploit_trend = pd.DataFrame(algos['our-method'].exploit_or_explore)
        print(df_our_method_explore_exploit_trend.shape)
        print(df_our_method_explore_exploit_trend.head(10))
        print(df_our_method_explore_exploit_trend[0].value_counts())
        sn.lineplot(data=df_our_method_explore_exploit_trend)
        df_our_method_explore_exploit_trend.to_csv(pngs_dir_name+"exploit_explore_data.csv")
        plt.ylim(-1,1.5)
        plt.xlim(-1,TRIALS+1)
        plt.xlabel("trials")
        plt.ylabel("is_exploit")
        plt.savefig(pngs_dir_name+"explore_vs_exploite_trend.pdf")


def plot_ctr(dict_cumm_rewards):
    df_ctr = pd.DataFrame(dict_cumm_rewards)
    df_ctr.to_csv(pngs_dir_name + "ctr_" + str(TRIALS) + ".csv")

    plt.clf()
    plt.figure(figsize=(5, 2.5), dpi=300)
    df_ctr = pd.DataFrame(dict_cumm_rewards)
    sn.lineplot(data=df_ctr)
    plt.xlabel("No. of trials * 100")
    plt.ylabel("Avg cumm. reward")
    plt.legend(prop={'size':6})
    plt.tight_layout()
    plt.savefig(pngs_dir_name+'sim_result_'+str(NO_ARMS)+'.pdf')




def dump_results(algos, path):
    frequency_list = []
    for algo in algos.keys():
        frequency_list.append({algo:algos[algo].arm_frequency})
    with open(path+'frequency.json', 'a') as f:
            json.dump(frequency_list, f)
    f.close()

def dump_algo_configurations(algos, path):
    for algo_name in algos:
        algo = algos[algo_name]
        algo.configs.get_str_configations(algo_name, path)


def get_grid_instance_id(params_list):
    # return str(params_list).strip('[]')
    return "_".join(map(str, params_list))


if __name__ == '__main__':

    pngs_dir_name = "plots/tuned_results/3/"+datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")

    # reward_fn = 1
    # algo_est = sed.EstDist(reward_fn)
    # algo_heu = sh.EstDistHeuristic(reward_fn)
    # # algo_est_cont = sedc.EstDistCont(1)
    # algo_true = std.TrueDist(reward_fn)
    # algo_single = snd.Single(reward_fn)
    # algo_metric = sm.TrueDistMetric(reward_fn)

    # Common parameters
    NO_ARMS = 50
    TRIALS = 100000
    NOISE = 1e-3

    # Parameters tuned for EstDist algorithm
    s_bias_EstDis = 0.25
    s_dist_EstDis = 0.5
    sampling_constant_EstDis = 1
    cont_disc_n_EstDist = 6
    neighbours = 10
    min_k = 5

    # Parameters tuned for TrueDist algorithm
    s_bias_TrueDist = 0.25
    s_dist_TrueDist = 0.07
    cont_disc_n_TrueDist = 6

    # Parameters tuned for Single algorithm
    # TODO confirm this

    # Parameters to tuned for the TrueDistMetric
    s_bias_TrueDistMetric = 0.25
    s_dist_TrueDistMetric = 10
    cont_disc_n_TrueDistMetric = 6

    str_id = get_grid_instance_id(["a", NO_ARMS, "t", TRIALS, "n", NOISE])
    pngs_dir_name = pngs_dir_name+"_"+str_id+"/"
    os.mkdir(pngs_dir_name)



    # algo_est_2 = sed.EstDist(2, con.Configs(TRIALS, NO_ARMS,0.25, 0.5, 1, NOISE, 6, 10))
    # # algo_heu_2 = sh.EstDistHeuristic(2)
    # algo_true_2 = std.TrueDist(2, con.Configs(TRIALS, NO_ARMS, 0.25, 0.07, None, NOISE, 6, None))
    # algo_single_2 = snd.Single(2, con.Configs(TRIALS, NO_ARMS, 0.25, None, None, NOISE, None, None))
    # algo_metric_2 = sm.TrueDistMetric(2, con.Configs(TRIALS, NO_ARMS, 0.25, 10, None, NOISE, 6, None))

    algo_est_2 = sed.EstDist(2, con.Configs(TRIALS, NO_ARMS, s_bias_EstDis, s_dist_EstDis,
                                            sampling_constant_EstDis, NOISE, cont_disc_n_EstDist, neighbours, min_k))
    algo_true_2 = std.TrueDist(2, con.Configs(TRIALS, NO_ARMS, s_bias_TrueDist, s_dist_TrueDist,
                                              None, NOISE, cont_disc_n_TrueDist, None, None))
    algo_single_2 = snd.Single(2, con.Configs(TRIALS, NO_ARMS, 0.25, None,  # tunning bias should not change the result!
                                              None, NOISE, None, None, None))
    algo_metric_2 = sm.TrueDistMetric(2, con.Configs(TRIALS, NO_ARMS, s_bias_TrueDistMetric, s_dist_TrueDistMetric,
                                                     None, NOISE, cont_disc_n_TrueDistMetric, None, None))

    # initialize GP-UCB
    # algo_gp_single_2 = gp_single.super_GPUCB(2, con.Configs(TRIALS, NO_ARMS, None, None, None, NOISE,2, None))


    algos = {#"Approx-Zooming-f-smooth":algo_est,
             #"Approx-Zooming-Heu-f-smooth": algo_heu,
             # "true-D-f-smooth":algo_true,
             # "single-f-smooth":algo_single,
             # "metric-D-f-smooth": algo_metric, # slivkins variation
             "Approx-Zooming": algo_est_2,
             # "Approx-Zooming-Heu-f-zigzag": algo_heu_2,
             "Approx-Zooming-True": algo_true_2,
             "Approx-Zooming-No-Arm-Similarity": algo_single_2,
             "Approx-Zooming-Similarity-Metric": algo_metric_2,
            # 'GP-No_Arm_Similarity': algo_gp_single_2
         }


    # for avg_count in range(0,5):
    for t in range(0, TRIALS):
        print('T === ', t)
        x_t = get_random_context()
        print('x_t ', x_t)

        # algo_est.get_reward(x_t,pngs_dir_name)
        # algo_heu.get_reward(x_t, pngs_dir_name)
        # algo_true.get_reward(x_t,pngs_dir_name)
        # algo_metric.get_reward(x_t,pngs_dir_name)
        # algo_single.get_reward(x_t, pngs_dir_name)
        algo_est_2.get_reward(x_t, pngs_dir_name)
        # algo_heu_2.get_reward(x_t, pngs_dir_name)
        algo_true_2.get_reward(x_t, pngs_dir_name)
        algo_metric_2.get_reward(x_t, pngs_dir_name)
        algo_single_2.get_reward(x_t, pngs_dir_name)
        # algo_gp_single_2.get_reward(x_t, pngs_dir_name)


    with open(pngs_dir_name + 'partition_distance_comparison_estimated.json', 'a') as f:
        json.dump('phase_index|tilde_dist|true_dist|arm_radius\n', f)
        print(algos["Approx-Zooming"])
        json.dump(algos["Approx-Zooming"].str_partition_distance_comparison_true, f)
    f.close()

    with open(pngs_dir_name + 'partition_distance_comparison_true.json', 'a') as f:
        json.dump('phase_index|true_dist|arm_radius\n', f)
        json.dump(algos["Approx-Zooming-True"].str_partition_distance_comparison_true, f)
    f.close()

    with open(pngs_dir_name + 'partition_distance_comparison_metric.json', 'a') as f:
        json.dump('phase_index|metric_dist|arm_radius\n', f)
        json.dump(algos["Approx-Zooming-Similarity-Metric"].str_partition_distance_comparison_true, f)
    f.close()


    f = open(pngs_dir_name+"partitions.txt", 'w')
    # f.write("Partitions with True Distance\n")
    # cpn.printTree(algo_true.root_not_flagged_partitions, f)
    # f.write('Partitions with Distance Metric\n')
    # cpn.printTree(algo_metric.root_not_flagged_partitions, f)
    # f.write('Partitions with No Distance\n')
    # cpn.printTree(algo_single.root_not_flagged_partitions, f)
    # f.write("\nOUR METHOD\n")
    # f.write("===NOT-FLAGGED===\n")
    # cpn.printTree(algo_est.root_not_flagged_partitions, f)
    # f.write("===FLAGGED===\n")
    # cpn.printTree(algo_est.root_flagged_partitions, f)
    # f.write("\nHeuristic\n")
    # f.write("===H-NOT-FLAGGED===\n")
    # cpn.printTree(algo_heu.root_not_flagged_partitions, f)
    # f.write("===H-FLAGGED===\n")
    # cpn.printTree(algo_heu.root_flagged_partitions, f)
    f.write("Partitions with True Distance"+"\n")
    cpn.printTree(algo_true_2.root_not_flagged_partitions, f)
    f.write('Partitions with Distance Metric'+'\n')
    cpn.printTree(algo_metric_2.root_not_flagged_partitions, f)
    f.write('Partitions with No Distance'+'\n')
    cpn.printTree(algo_single_2.root_not_flagged_partitions, f)
    f.write("\nOUR METHOD"+"\n")
    f.write("===NOT-FLAGGED===\n")
    cpn.printTree(algo_est_2.root_not_flagged_partitions, f)
    f.write("===FLAGGED==="+"\n")
    cpn.printTree(algo_est_2.root_flagged_partitions, f)
    # f.write("\nHeuristic" + append_str + "\n")
    # f.write("===H-NOT-FLAGGED===\n")
    # cpn.printTree(algo_heu_2.root_not_flagged_partitions, f)
    # f.write("===H-FLAGGED===" + append_str + "\n")
    # cpn.printTree(algo_heu_2.root_flagged_partitions, f)


    f.close()

    plot_graphs(algos)

    print(NO_ARMS, TRIALS, NOISE)

    dump_results(algos, pngs_dir_name)
    dump_algo_configurations(algos, pngs_dir_name)





