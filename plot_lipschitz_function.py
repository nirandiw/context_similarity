import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import cm
import seaborn
np.set_printoptions(precision=8)

fig = plt.figure(figsize=(5, 3), dpi=300)
ax = fig.gca(projection='3d')

# Make data.
gap = 1
X = np.arange(0,1,0.1)
Y = np.arange(0,10,1)
X, Y = np.meshgrid(X, Y)
abs_diff = abs(X-((Y+0.5) / 10.0))
print(abs_diff)
Z = np.divide(1,abs_diff+1)
max_Z = np.max(Z)
print(max_Z)
# Z = 1/(1+2**(abs_diff))
print Z-0.05
# Z = np.where((Z - 0.05)<1e-8, gap, 0)
print(Z)
print(Z.shape)
# Plot the surface.
# surf = ax.plot_surface(X, Y, np.divide(Z, max_Z), cmap=cm.coolwarm)
# # seaborn.heatmap(np.divide(Z, max_Z))
#
# # Add a color bar which maps values to colors.
# fig.colorbar(surf, shrink=0.5, aspect=5)
# ax.set_xlabel('Context')
# ax.set_ylabel('Arm Type')
# ax.set_zlabel('Reward')
# plt.show()
# plt.savefig('reward_func.pdf')
#
# plt.clf()
# fig = plt.figure(figsize=(5, 3), dpi=300)
# ax = fig.gca(projection='3d')
def get_function_2_reward(context, y_t, lips=1):
    # y_t = np.divide(y_t, 50)
    y_t_mean_min = float("inf")

    y_t_mean_min_0 = abs(y_t - 0)
    y_t_mean_min_0_5 = abs(y_t - 0.5)
    y_t_mean_min_1 = abs(y_t - 1)

    min_array = np.minimum(y_t_mean_min_0,y_t_mean_min_0_5 )
    min_array = np.minimum(min_array,y_t_mean_min_1 )
    y_t_mean= 1- (lips * abs(context- (4* min_array )))
    return y_t_mean



def get_function_1_reward(context, y_t, lips=1):
    y_t_mean =  1-(lips* abs(context-y_t))
    return y_t_mean

X = np.arange(0,1,0.001)
Y = np.arange(0,1,0.001)
X, Y = np.meshgrid(X, Y)
print(X)
print(Y)
Z = get_function_2_reward(X, Y)
print(Z)
print(Z.shape)
surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)
fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_xlabel('Context')
ax.set_ylabel('Continuos Arm Space')
ax.set_zlabel('Rewards')
plt.show()
plt.savefig('reward_func_1.pdf')





