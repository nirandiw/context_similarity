import  json
import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import pandas as pd
import configurations as configs
import seaborn as sn
sn.set_style("darkgrid")
folder =  '2019-05-24 22-50-07-513932' #'2019-05-23 22-29-11-668299'
# path = 'plots//2019-05-23 23-41-37-568543/frequency.json'
path = 'plots//'+folder+'/frequency.json'
path2 = 'plots//'+folder+'/ctr_100000.csv'

def plot_ctr(df_ctr):
    plt.clf()
    plt.figure(figsize=(5, 2.5), dpi=300)
    # df_ctr = pd.DataFrame(dict_cumm_rewards)
    sn.lineplot(data=df_ctr)
    plt.xlabel("No. of trials * 100")
    plt.ylabel("Avg cumm. reward")
    plt.legend(prop={'size':8})
    plt.tight_layout()
    plt.savefig('plots/tuned_results/3/2019-10-28 07-13-14_a_200_t_100000_n_0.01/sim_result_200.pdf')


df_ctr = pd.read_csv('plots/tuned_results/3/2019-10-28 07-13-14_a_200_t_100000_n_0.01/ctr_100000.csv') #path2
print(df_ctr.columns)
# df_ctr.columns = ['Unnamed', 'mm','Approx-Zooming-Similarity-Metric',
#                                   'Approx-Zooming-f-smooth','Approx-Zooming',
#        'single-f-smooth', 'Approx-Zooming-No-Arm-Similarity','true-D-f-smooth','Approx-Zooming-True'
#        ]
print(df_ctr.columns)
# plot_ctr(df_ctr[])

df_ctr.columns = ['plop','Sim-UCB','Sim-UCB-Similarity-Metric',
                 'Sim-UCB-No-Arm-Similarity','Sim-UCB-True']

print(df_ctr.columns)

print(df_ctr.shape)

# plot_ctr(df_ctr[['Approx-Zooming','Approx-Zooming-Similarity-Metric',
#                  'Approx-Zooming-No-Arm-Similarity','Approx-Zooming-True']])

plot_ctr(df_ctr[['Sim-UCB','Sim-UCB-Similarity-Metric',
                 'Sim-UCB-No-Arm-Similarity','Sim-UCB-True']])

# f = open(path, 'r')
#
# frequency_list = json.load(f)



#
# def plot_arm_frequency(f, file_name, flag):
#     plt.clf()
#     fig = plt.figure(figsize=(5, 1.7), dpi=300)
#     # f = inst_algo.arm_frequency
#     #print("Frequency", f)
#     e = configs.T/4
#     step = configs.T/4
#     col = 1
#     cbar_ax = fig.add_axes([0.6, .4, .3, .03])
#     for s in range(0, len(f), step):
#         heatmap_data_all = np.zeros((configs.no_A, 10))
#         heatmap_data_explore = np.zeros((configs.no_A,10))
#         heatmap_data_exploit = np.zeros((configs.no_A,10))
#         for _tuple in f[s:e]:
#             a = _tuple[1]
#             cont = int(_tuple[0]/0.1)
#             heatmap_data_all[(a), cont] = heatmap_data_all[a, cont] + 1
#             if _tuple[2] == 1:
#                 heatmap_data_exploit[(a), cont] = heatmap_data_exploit[a, cont] + 1
#             if _tuple[2] == 0:
#                 heatmap_data_explore[(a), cont] = heatmap_data_explore[a, cont] + 1
#         e = min(e+step, len(f))
#         # print(heatmap_data)
#         sb = plt.subplot(1,4,col)
#         if flag == 2:
#             sn.heatmap(heatmap_data_all, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"}, yticklabels=False, xticklabels=False)
#         elif flag == 1:
#             sn.heatmap(heatmap_data_exploit, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"})
#         elif flag == 0:
#             sn.heatmap(heatmap_data_explore, cbar=s == 0, cbar_ax=None if s else cbar_ax, cbar_kws={"orientation": "horizontal"})
#         sb.set_xlabel("t="+str(col))
#         col = col + 1
#     fig.text(0.5, 0.0, 'Context Interval', ha='center')
#     fig.text(0.0, 0.5, 'Finite Arms', va='center', rotation='vertical')
#     # fig.suptitle('Selected Arm Type Frequency_'+str(flag))
#     plt.tight_layout()
#     plt.savefig("arm_frequency_"+file_name+"_"+str(flag)+".pdf")
#
#     # if len(f) > 0:
#     #     histogram_data_arms = list(zip(*f)[1])
#     #     histogram_data_context = list(zip(*f)[0])
#     #     plt.clf()
#     #     ax = plt.subplot(2,1,1)
#     #     sn.distplot(histogram_data_arms, kde=False, rug=True)
#     #     ax.set_title("arm selection distribution")
#     #     ax =plt.subplot(2,1,2)
#     #     sn.distplot(histogram_data_context, kde=False, rug=True)
#     #     ax.set_title("context distribution")
#     #     plt.savefig(pngs_dir_name+"total_arm_f_"+file_name+".pdf")
#
#
#
# for n_dict in frequency_list:
#     key = n_dict.keys()[0]
#     vals = n_dict.values()[0]
#     plot_arm_frequency(vals, key, 2)
