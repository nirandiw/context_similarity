# from statsmodels.nonparametric.kernel_regression import KernelReg
import math
import random

import numpy as np
from sklearn.neighbors import KNeighborsRegressor

# import configurations as configs
import context_partition_node as cpn
import partition as par
import super_dist as sd


class EstDistHeuristic:

    def __init__(self, reward_function, congiurations):
        self.configs = congiurations
        self.neighbours = 25
        self.root_flagged_partitions = cpn.Node((0.0, 1.0), [])
        self.root_flagged_partitions.insertlnOrder((0.0, 0.5),[par.Partition((0.0, 0.5), range(0, self.configs.no_A),
                                                                             self.neighbours*self.configs.no_A*self.configs.sampling_constant,
                                                                             self.configs.starting_heu_distance_threshold,
                                                                             self.configs.starting_heu_bias, self.configs.T)])
        self.root_flagged_partitions.insertlnOrder((0.5, 1.0), [par.Partition((0.5, 1.0),range(0, self.configs.no_A),
                                                                              self.neighbours*self.configs.no_A,
                                                                              self.configs.starting_heu_distance_threshold,
                                                                              self.configs.starting_heu_bias, self.configs.T)])
        self.root_not_flagged_partitions = cpn.Node((0.0, 1.0), [])
        self.is_not_init = False
        self.cum_r_list = []
        self.arm_frequency = []
        self.temp_T =0
        self.exploit_or_explore =[]
        self.reward_function_flag = reward_function
        self.global_explore_samples = {}



    def get_tilde_distance(self, _center_samples, _a_samples, _context_width):
        arr_center_samples = np.array(_center_samples)
        arr_a_samples = np.array(_a_samples)

        # tilde_f_c = smooth.NonParamRegression(arr_center_samples[:, 0], arr_center_samples[:, 1],
        #                                       method=npr_methods.SpatialAverage(),
        #                                       bandwidth=(_context_width[1]-_context_width[0])*0.2)
        # tilde_f_c.fit()
        # tilde_f_a = smooth.NonParamRegression(arr_a_samples[:, 0], arr_a_samples[:, 1],
        #                                       method=npr_methods.SpatialAverage(),
        #                                       bandwidth=(_context_width[1]-_context_width[0])*0.2)
        # tilde_f_a.fit()

        # delta_c = abs(_context_width[1]-_context_width[0])
        # if int(26*delta_c) > 5:
        #     neighbours = int(26*delta_c)
        # else:
        #     neighbours = 5

        # avg_samples = int(np.divide(self.neighbours, math.sqrt(_context_width[1]-_context_width[0]*2)))
        c_neigh = KNeighborsRegressor(n_neighbors=self.neighbours)
        c_neigh.fit(arr_center_samples[:, 0].reshape(-1,1), arr_center_samples[:, 1])
        a_neigh = KNeighborsRegressor(n_neighbors=self.neighbours)
        a_neigh.fit(arr_a_samples[:,0].reshape(-1,1), arr_a_samples[:,1])

        data_predict = np.linspace(_context_width[0], _context_width[1], self.configs.discrete_est_n)

        # mean_c = tilde_f_c(data_predict)
        # mean_a = tilde_f_a(data_predict)
        mean_c = c_neigh.predict(data_predict.reshape(-1,1))
        mean_a = a_neigh.predict(data_predict.reshape(-1,1))
        assert mean_c.shape[0] == self.configs.discrete_est_n
        mean_diff = np.subtract(mean_c, mean_a)
        squared_diff = np.square(mean_diff)
        tilde_D_c_a = math.sqrt(np.divide(np.sum(squared_diff), self.configs.discrete_est_n))
        if np.isnan(tilde_D_c_a):
            print(mean_c, mean_a, mean_diff, squared_diff, tilde_D_c_a, self.configs.discrete_est_n)
            exit(-1)
        #print("Tilde D(c,a)", tilde_D_c_a)
        return tilde_D_c_a

    def get_new_rho_tilde_distance_avg(self, _new_rho, c, parent):
        tilde_d = 0.0
        for a in _new_rho.A:
            if a is not c:
                tilde_d = tilde_d + self.get_tilde_distance(parent.explore_samples[c], parent.explore_samples[a],
                                                            _new_rho.c)
        if len(_new_rho.A) == 1:
            assert c == _new_rho.A[0]
            return 0
        else:
            return tilde_d / (len(_new_rho.A) - 1.0)

    def do_partiion(self, _rho, path):
        # print("Old Trees")
        # cpn.printTree(self.root_flagged_partitions, None)
        # cpn.printTree(self.root_not_flagged_partitions, None)

        random.shuffle(_rho.A)
        new_partitions = []
        centers = {_rho.A[0]: []}

        for a in _rho.A[1:]:
            center = self.find_closest_center(centers, a, _rho.c, _rho.distance_threshold, _rho)
            if center is None:
                centers[a] = []
        for a in _rho.A:
            center = self.find_closest_center(centers, a, _rho.c, _rho.distance_threshold, _rho)
            if center is None:
                print("Logic error in clustering")
                exit(-1)
            else:
                centers[center].append(a)

        for center in centers.keys():
            new_partition = par.Partition(_rho.c, centers[center], self.neighbours*len(centers[center])*self.configs.sampling_constant,
                                          _rho.distance_threshold,
                                          _rho.bias,
                                          self.configs.T)  # TODO get the correct thresholds and bias?

            # new_partition.set_exploit_samples_from_parent(_rho)
            new_partition.set_parent(_rho.parent)
            new_partitions.append(new_partition)

            rho_tilde_distance = self.get_new_rho_tilde_distance_avg(new_partition, center, _rho)
            rho_true_distance = sd.get_rho_true_distance(new_partition, center, self.reward_function_flag)
            f = open(path+"partition_distance_comparison_estimated_heu.txt", 'a')
            f.write(str(self.temp_T) + '|' + str(rho_tilde_distance) + '|' + str(rho_true_distance) + '|'
                    +str(_rho.distance_threshold)+'\n')
            f.close()

        self.root_not_flagged_partitions.insertlnOrder(_rho.c, new_partitions)
        self.root_flagged_partitions.remove(_rho)

    def explore(self, _x_t, _rho, path):
        self.exploit_or_explore.append(0)
        y_t = _rho.play_rr_arm() #random.choice(_rho.A)
        print("**Explore y_t", y_t)
        self.arm_frequency.append((_x_t, y_t,0))
        pi_t = sd.get_noisy_reward(_x_t, y_t,self.reward_function_flag)

        if y_t in _rho.explore_samples.keys():
            _rho.explore_samples[y_t].append([_x_t, pi_t])
        else:
            _rho.explore_samples[y_t] = [[_x_t, pi_t]]

        self.update_glabal_explore_samples(pi_t, _x_t, y_t)

        _rho.explore_samples_count = _rho.explore_samples_count + 1
        if _rho.explore_samples_count > _rho.sample_threshold: # TODO change in the heuristic setting
        # num_global_rel_samples, global_rel_samples = self.get_rel_global_samples(_rho.c, _rho.A)
        # if num_global_rel_samples > _rho.sample_threshold:
            print("###", _rho.sample_threshold, _rho.explore_samples_count, len(_rho.A))
            self.do_partiion(_rho, path)
        return pi_t

    def update_glabal_explore_samples(self, pi_t, x_t, y_t):
        if y_t in self.global_explore_samples.keys():
            self.global_explore_samples[y_t].append([x_t, pi_t])
        else:
            self.global_explore_samples[y_t] = [[x_t, pi_t]]

    def exploit_ucb(self, _x_t, _partitions):
        self.exploit_or_explore.append(1)
        max_rho = _partitions[0]
        max_I_t = _partitions[0].get_I_t()

        for p in _partitions[1:]:
            if p.get_I_t() > max_I_t:
                max_I_t = p.get_I_t()
                max_rho = p

        y_t = max_rho.play_rr_arm() #random.choice(max_rho.A)
        print("**Exploit y_t", y_t)
        self.arm_frequency.append((_x_t,y_t,1))
        pi_t = sd.get_noisy_reward(_x_t, y_t, self.reward_function_flag)
        max_rho.update(_x_t, y_t, pi_t)

        self.update_glabal_explore_samples(pi_t, _x_t, y_t)

        if max_rho.confidence_radius < max_rho.bias:
            new_partition_1 = par.Partition((max_rho.c[0], (max_rho.c[1] + max_rho.c[0]) / 2.0),
                                            max_rho.A, max_rho.sample_threshold,
                                            max_rho.distance_threshold / 2.0, max_rho.bias / 2.0,
                                            self.configs.T)
            new_partition_2 = par.Partition(((max_rho.c[1] + max_rho.c[0]) / 2.0, max_rho.c[1]),
                                            max_rho.A, max_rho.sample_threshold,
                                            max_rho.distance_threshold / 2.0, max_rho.bias / 2.0,
                                            self.configs.T)
            new_partition_1.set_parent(max_rho)
            new_partition_2.set_parent(max_rho)
            if max_rho.A > 1:
                self.root_flagged_partitions.insertlnOrder(new_partition_1.c, [new_partition_1])
                self.root_flagged_partitions.insertlnOrder(new_partition_2.c, [new_partition_2])
            else:
                self.root_not_flagged_partitions.insertlnOrder(new_partition_1.c, [new_partition_1])
                self.root_not_flagged_partitions.insertlnOrder(new_partition_2.c, [new_partition_2])
            self.root_not_flagged_partitions.remove(max_rho)
        return pi_t

    # Give priority to the partition with the longest length (uniform sampling)
    # search the binary and find the first instance with a flagged partition. It is the longest length
    def is_flagged(self, _x_t, _root_flagged_partitions):
        _is_flag, _prior_rel_rhos = _root_flagged_partitions.find_flagged(_x_t)
        return _is_flag, _prior_rel_rhos


    def get_reward(self, x_t, path):
        tmp_stop_condition = False
        flagged, relevant_rhos = self.is_flagged(x_t, self.root_flagged_partitions)
        if flagged:
            # self.is_not_init = False
            r_t = self.explore(x_t, random.choice(relevant_rhos.data_rhos), path)
        else:
            self.is_not_init = True
            relevant_rhos = sd.get_relevant_rhos(x_t, self.root_not_flagged_partitions)
            r_t = self.exploit_ucb(x_t, relevant_rhos)
            self.temp_T = self.temp_T + 1
        self.cum_r_list.append(r_t)
        self.temp_T = self.temp_T+1

        print(r_t)
        #print("XXX")
        #cpn.printTree(self.root_not_flagged_partitions, None)
        #print("YYY")
        #cpn.printTree(self.root_flagged_partitions, None)

        return tmp_stop_condition

    def find_closest_center(self, centers, a, cp, new_eta_2, _rho):
        min_dist = float('inf')
        min_c = None
        for c in centers.keys():

            num_global_rel_samples, relevant_global_samples = self.get_rel_global_samples( cp, [c, a])
            tilde_distance = self.get_tilde_distance(_rho.explore_samples[c]+relevant_global_samples[c],
                                                     _rho.explore_samples[a]+relevant_global_samples[a], cp)

            # sd.get_distance(c, a, cp)
            # print('c =', c, ': ',len(_rho.explore_samples[c]),' a = ', a,': ',len(_rho.explore_samples[a]))

            if tilde_distance <= new_eta_2:
                if tilde_distance < min_dist:
                    min_dist = tilde_distance
                    min_c = c
        return min_c

    def get_rel_global_samples(self, context, arms):
        rel_global_samples = {}
        rel_global_samples_counter = 0
        for arm_id in arms:
            if arm_id in self.global_explore_samples.keys():
                for point in self.global_explore_samples[arm_id]:
                    if context[0] <= point[0] <= context[1]:
                        if arm_id in rel_global_samples.keys():
                            rel_global_samples[arm_id].append(point)
                        else:
                            rel_global_samples[arm_id] = [point]
                        rel_global_samples_counter = rel_global_samples_counter+1
        return rel_global_samples_counter, rel_global_samples


