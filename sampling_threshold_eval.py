import configurations as configs
import super_dist as sd
import random
import numpy as np
import run
import sim_true_distance as std
import sim_estimated_dist as sed

partition_list =[[] for i in range(0,10)]
# print(partition_list)

for a in range(1, configs.no_A+1):
    idx = a % 10
    partition_list[idx-1].append(a)

# print(partition_list)

no_samples = 1000



dict_samples={}

for i in range(0, no_samples):
    x_t = run.get_random_context()
    for pl in partition_list:
        y_t = random.choice(pl)
        pi_t = sd.get_noisy_reward(x_t, y_t)
        if y_t in dict_samples.keys():
            dict_samples[y_t].append([x_t, pi_t])
        else:
            dict_samples[y_t] = [[x_t, pi_t]]


arra = np.zeros((configs.no_A, configs.no_A), dtype=np.float64)

for i in range(0, configs.no_A):
    for j in range(0, configs.no_A):
        tilde_dist = sed.EstDist().get_tilde_distance(dict_samples[i + 1], dict_samples[j + 1], (0, 1))
        true_dist = sd.get_distance(i+1, j+1, (0,1))
        print(tilde_dist)
        arra[i][j] = abs(tilde_dist-true_dist)

print(arra)
print(np.sum(arra, (0,1)))
# print(dict_samples)