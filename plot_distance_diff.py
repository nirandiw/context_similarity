import pandas as pd
import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import seaborn as sns

df_estimated = pd.read_csv('partition_distance_comparison_estimated.txt', sep='|', dtype={'tilde_dist':float,'true_dist':float,'phase_index':int, 'arm_radius':float})
df_true = pd.read_csv('partition_distance_comparison_true.txt', sep='|', dtype={'phase_index':int,'true_dist':float,'arm_radius':float})


df_estimated['diff'] = df_estimated['true_dist']- df_estimated['tilde_dist']

sns.lineplot(data=df_estimated[['tilde_dist','true_dist','arm_radius' ]])
plt.xlabel("time step")
plt.ylabel("distance")
plt.savefig("distance_diff_estimated")

plt.clf()
sns.lineplot(data=df_true[['true_dist','arm_radius' ]])
plt.savefig("distance_diff_true")
