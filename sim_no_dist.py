from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pylab as plt
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor
import configurations as configs
import random
import super_dist as sd
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
import datetime
import os
import time
import pandas as pd
import seaborn as sn
from multiprocessing import Pool
from multiprocessing import Process


#TODO (1) Thompson sampling (2) slivkins for arms seperately (2) a prior with independance

class Arm:
    def __init__(self, _id, _beta, _dir):
        self.dir = _dir
        self.id = _id
        self.contexts = []
        self.rewards = []
        self.beta = _beta
        # print(self.contexts.shape)
        # print(self.rewards.shape)
        self.kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
        #self.kernel = C(0.9,(0.8, 1))* RBF(0.2, (0.1, 0.3))  # TODO check the bandwidth , pick a good kernel
        #  RBF high gamma makes the curve smoother
        self.gp = GaussianProcessRegressor(kernel=self.kernel)#, alpha=0.01)

    def predict_reward(self, _context):
        predict_arr = np.atleast_2d([_context]).T

        # print(predict_arr.shape)
        _mu, _sigma = self.gp.predict(predict_arr, return_std=True)
        # print(self.id, _mu, _sigma)
        # print("mu shape", _mu.shape, "sigma shape", _sigma.shape)
        payoff = _mu + _sigma * np.sqrt(self.beta)
        # print("a_r ", payoff, payoff.shape)
        return payoff[0]

    def update(self, _x, _r):
        self.contexts.append(_x)
        self.rewards.append(_r)
        print(str(self.id)+" Shapes", len(self.contexts), len(self.rewards))
        # print(self.contexts)
        # print(self.rewards)
        self.gp.fit(np.atleast_2d(self.contexts).T, self.rewards)


    def plot(self):
        X = np.atleast_2d(np.linspace(0, 1, 5)).T

        print("X_", X)
        print(self.contexts, self.rewards)
        y_pred, y_std = self.gp.predict(X, return_std=True)  # dump the data and plot it

        # Illustration
        plt.scatter(self.contexts, self.rewards, c='r')

        plt.plot(X, y_pred)
        y_pred = y_pred.reshape(-1, 1)
        # print("y_pred", y_pred, y_pred.shape)
        # print("y_std",y_std)
        plt.fill_between(X[:,0], y_pred[:,0] - y_std, y_pred[:,0] + y_std,
                         alpha=0.5, color='b')
        plt.xlim(X.min(), X.max())
        plt.tight_layout()
        plt.title("ID" + str(self.id))
        plt.savefig(self.dir + datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S-%f"))
        plt.close()

class GPUCB():
    def __init__(self, _dirname, arms= configs.no_A, beta=100):
        self.dirname = _dirname
        self.beta = beta
        self.dict_arms = {}
        for _id in range(0, arms):
            new_arm = Arm(_id, self.beta, self.dirname)
            self.dict_arms[_id] = new_arm

        self.arm_frequency =[]
        self.cum_r_list = []

        self.get_reward_elapsed_time = []
        self.update_elapsed_time = []

    def predict_reward_by_kernal(self, arm_range):
        return [self.dict_arms[i].predict_reward(x_t) for i in range(arm_range[0], arm_range[1])]

    def get_reward(self, x_t):
        start_time = time.time()

        # no_kernals = 10 # number of processing
        # p = Pool(no_kernals)
        # linspace = np.int64(np.floor(np.linspace(0, configs.no_A, no_kernals + 1)))
        # arm_ranges = [(linspace[i], linspace[i + 1]) for i in range(no_kernals)]
        # print(arm_ranges)

        # a function and a lambda function is both okay
        # def predict_reward_by_kernal(arm_range):
        #     return [self.dict_arms[i].predict_reward(x_t) for i in range(arm_range[0], arm_range[1])]
        # estimated_rewards = p.map(self.predict_reward_by_kernal, arm_ranges)
        
        # estimated_rewards = p.map(
        #    lambda arm_range: [self.dict_arms[i].predict_reward(x_t) for i in range(arm_range[0], arm_range[1])],
        #    arm_ranges) # lambda method, you got a list with length no_kernals
        # arr_rewards = np.asarray(estimated_rewards).flatten() # concatenation of lists


        # your previous implement
        estimated_rewards = [self.dict_arms[i].predict_reward(x_t) for i in range(0, configs.no_A)]
        arr_rewards = np.array(estimated_rewards)

        # assert arr_rewards.shape[1] == 1
        max_reward_arm_indeces = np.argwhere(arr_rewards == np.amax(arr_rewards))[:,0]
        # print("Max reward arm indexes", max_reward_arm_indeces)
        max_reward_arm_idx = np.random.choice(max_reward_arm_indeces)
        if estimated_rewards[max_reward_arm_idx] < 0 :
            print("Check")

        y_t = max_reward_arm_idx
        print(y_t)
        elapsed_time = time.time() - start_time
        self.get_reward_elapsed_time.append(elapsed_time)

        start_time = time.time()
        self.arm_frequency.append((x_t, y_t))
        pi_t = sd.get_noisy_reward(x_t, y_t)
        self.dict_arms[y_t].update(x_t, pi_t)
        self.cum_r_list.append(pi_t)
        print("Selected", y_t, pi_t)
        elapsed_time = time.time() - start_time
        self.update_elapsed_time.append(elapsed_time)
        return pi_t


def get_random_context():
    return random.uniform(0, 1)


if __name__ == '__main__':
    pngs_dir_name = "pngs_" + datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S-%f") + "/"
    os.mkdir(pngs_dir_name)

    agent = GPUCB(pngs_dir_name)

    for t in range(0, configs.T):
        print('T === ', t)
        x_t = get_random_context()
        print('x_t ', x_t)
        r_t = agent.get_reward(x_t)
        print(r_t)

    # print(agent.cum_r_list)
    #
    # df = pd.DataFrame({'get_reward':agent.get_reward_elapsed_time, 'update':agent.update_elapsed_time})
    # print(df.head())
    #
    # ax = sn.lineplot(data=df)
    # plt.xlabel("trial")
    # plt.ylabel("time")
    # plt.savefig("100 Elapsed time for GP single - parallel")

    for a in agent.dict_arms.values():
       a.plot()
