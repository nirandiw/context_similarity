import random
import numpy as np
import partition as par
import math
# import configurations as configs
import context_partition_node as cpn
import super_dist as sd

np.random.seed(500)


class TrueDistMetric:
    def __init__(self, reward_function, configurations):
        self.configs = configurations
        self.root_not_flagged_partitions = cpn.Node((0.0, 1.0), [])
        self.root_not_flagged_partitions.insertlnOrder((0.0, 0.5), [
            par.Partition((0.0, 0.5), range(0, self.configs.no_A), None,
                          self.configs.starting_X_distance_threshold, self.configs.starting_X_bias,
                          self.configs.T)])
        self.root_not_flagged_partitions.insertlnOrder((0.5, 1.0), [
            par.Partition((0.5, 1.0), range(0, self.configs.no_A), None,
                          self.configs.starting_X_distance_threshold, self.configs.starting_X_bias,
                          self.configs.T)])
        self.cum_r_list = []
        # self.is_not_init = False
        self.arm_frequency = []
        self.temp_t = 1.0
        self.ctr = 0
        self.cumm_reward = 0
        self.temp_T = 0
        self.reward_function_flag = reward_function
        self.str_partition_distance_comparison_true = []

    def find_closest_center(self, centers, a, context_width, new_dist):
        min_dist = float('inf')
        min_c = None
        for c in centers.keys():
            print('c =', c, ' a = ', a)
            true_distance = sd.get_distance_matric(c, a, context_width,self.reward_function_flag, self.configs.no_A)
            if true_distance <= new_dist:
                if true_distance < min_dist:
                    min_dist = true_distance
                    min_c = c
        return min_c

    def do_partiion(self, _rho,path):
        print("Old Trees")
        cpn.printTree(self.root_not_flagged_partitions, None)

        new_context_partitions = [(_rho.c[0], (_rho.c[1] + _rho.c[0]) / 2.0),
                                  ((_rho.c[1] + _rho.c[0]) / 2.0, _rho.c[1])]

        # print("old distance", _rho.distance_threshold)
        new_eta_2 = np.divide(_rho.distance_threshold, 2.0)
        # print("new distance ", new_eta_2)

        # _rho.A.sort()
        new_arm_partitions =[_rho.A[:len(_rho.A)//2], _rho.A[len(_rho.A)//2:]]

        for cp in new_context_partitions:
            random.shuffle(_rho.A)
            new_partitions = []
            centers = {_rho.A[0]: []}

            for a in _rho.A[1:]:
                center = self.find_closest_center(centers, a, cp, new_eta_2)
                if center is None:
                    centers[a] = []
            for a in _rho.A:
                center = self.find_closest_center(centers, a, cp, new_eta_2)
                if center is None:
                    print("Logic error in clustering")
                    exit(-1)
                else:
                    centers[center].append(a)

            for center in centers.keys():
                # centers[center].append(center)
            # for arm_partition in new_arm_partitions:
                new_partition = par.Partition((cp[0], cp[1]), centers[center], None,   new_eta_2,
                                              _rho.bias / 2.0, self.configs.T)  # TODO get the correct thresholds and bias?
                # new_partition.set_exploit_samples_from_parent(_rho)
                new_partitions.append(new_partition)

                rho_metric_distance = sd.get_rho_metric_distance(new_partition, center, self.reward_function_flag, self.configs.no_A)
                # f = open(path+"partition_distance_comparison_metric.txt", 'a')
                # f.write(str(self.temp_T) + '|' + str(rho_metric_distance) +'|'+'-'+ '\n')
                # f.close()
                self.str_partition_distance_comparison_true.append(str(self.temp_T) + '|' + str(rho_metric_distance) +'|'+str(new_eta_2)+ '\n')


            self.root_not_flagged_partitions.insertlnOrder(cp, new_partitions)
            # print("New context paritions ", cp)
            # print("root_not_flagged_partitions")
            # cpn.printTree(self.root_not_flagged_partitions)

        # print("root_flagged_partitions")
        # cpn.printTree(root_flagged_partitions)

    def exploit_ucb(self, _x_t, _partitions,path):
        max_rho = _partitions[0]
        max_I_t = _partitions[0].get_I_t()

        for p in _partitions[1:]:
            if p.get_I_t() > max_I_t:
                max_I_t = p.get_I_t()
                max_rho = p

        y_t = max_rho.play_rr_arm() #random.choice(max_rho.A)
        print("Exploit y_t", y_t)
        self.arm_frequency.append((_x_t, y_t, 1))
        pi_t = sd.get_noisy_reward(_x_t, y_t, self.reward_function_flag, self.configs.no_A, self.configs.noise)
        max_rho.update(_x_t, y_t, pi_t)

        if max_rho.confidence_radius < max_rho.bias:
            self.do_partiion(max_rho, path)
            self.root_not_flagged_partitions.remove(max_rho)
        return pi_t

    # Give priority to the partition with the longest length (uniform sampling)
    # search the binary and find the first instance with a flagged partition. It is the longest length
    def is_flagged(self, _x_t, _root_flagged_partitions):
        _is_flag, _prior_rel_rhos = _root_flagged_partitions.find_flagged(_x_t)
        return _is_flag, _prior_rel_rhos

    def get_reward(self, x_t, path):
        relevant_rhos = sd.get_relevant_rhos(x_t, self.root_not_flagged_partitions)
        r_t = self.exploit_ucb(x_t, relevant_rhos, path)
        print("Reward ", r_t)
        self.cum_r_list.append(r_t)

        self.temp_T = self.temp_T + 1

        print("CTR")
        self.cumm_reward = self.cumm_reward + r_t
        self.ctr = self.cumm_reward / self.temp_t
        self.temp_t = self.temp_t + 1
        print(self.ctr)
