import numpy as np
from matplotlib import pylab as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
import super_dist as sd
# import configurations as configs


class GPUCB(object):

    def __init__(self, reward_function, _configs, beta=100., ):
        '''
        meshgrid: Output from np.methgrid.
        e.g. np.meshgrid(np.arange(-1, 1, 0.1), np.arange(-1, 1, 0.1)) for 2D space
        with |x_i| < 1 constraint.
        environment: Environment class which is equipped with sample() method to
        return observed value.
        beta (optional): Hyper-parameter to tune the exploration-exploitation
        balance. If beta is large, it emphasizes the variance of the unexplored
        solution solution (i.e. larger curiosity)
        '''
        #self.meshgrid = np.array(meshgrid)
        #self.environment = environment
        self.beta = beta
        self.configs = _configs

        #self.X_grid = self.meshgrid.reshape(self.meshgrid.shape[0], -1).T
        self.mu = np.zeros(self.configs.gp_context_n)
        self.sigma = np.array([0.01 for _ in range(self.configs.gp_context_n)])
        self.X = []
        self.Y = []
        self.kernel =  RBF(1, (1, 1)) # C(1.0, (1e-3, 1e3)) *
        self.gp = GaussianProcessRegressor(kernel=self.kernel)


    def ucb(self, x):
        x_mu, x_sigma = self.gp.predict(np.reshape(x, (1,-1)), return_std=True)
        ucb = x_mu + x_sigma * np.sqrt(self.beta)
        print('Arm UCB', ucb)
        return ucb

    def update(self, x_t, r_t):
        # grid_idx = self.argmax_ucb()
        # self.sample(self.X_grid[grid_idx])
        #gp = GaussianProcessRegressor()
        self.X.append(x_t)
        self.Y.append(r_t)
        self.gp.fit(np.reshape(self.X, (-1,1)), self.Y)
        #self.mu, self.sigma = gp.predict(self.X, return_std=True)

    # def sample(self, x):
    #     #t = self.environment.sample(x)
    #     self.X.append(x)
    #     self.Y.append(t)

    # def plot(self):
    #     fig = plt.figure()
    #     ax = Axes3D(fig)
    #     ax.plot_wireframe(self.meshgrid[0], self.meshgrid[1],
    #                       self.mu.reshape(self.meshgrid[0].shape), alpha=0.5, color='g')
    #     ax.plot_wireframe(self.meshgrid[0], self.meshgrid[1],
    #                       self.environment.sample(self.meshgrid), alpha=0.5, color='b')
    #     ax.scatter([x[0] for x in self.X], [x[1] for x in self.X], self.Y, c='r',
    #                marker='o', alpha=1.0)
    #     plt.savefig('fig_%02d.png' % len(self.X))



class super_GPUCB:

    def __init__(self, reward_function, configurations):
        self.configs = configurations
        self.armgp_list = []
        for arm in range(0, self.configs.no_A):
            self.armgp_list.append(GPUCB(reward_function, self.configs))

        self.cum_r_list = []
        self.arm_frequency = []
        self.reward_function_flag = reward_function

    def get_reward(self, x_t, path):
        y_t = self.super_argmax_ucb(x_t)
        print("**Selected y_t", y_t)
        self.arm_frequency.append((x_t, y_t, 0))
        pi_t = sd.get_noisy_reward(x_t, y_t, self.reward_function_flag)
        self.armgp_list[int(y_t)].update(x_t, pi_t)
        self.cum_r_list.append(pi_t)
        print(pi_t)


    def super_argmax_ucb(self, x_t):
        ucb_list = [arm.ucb(x_t)for arm in self.armgp_list]
        return np.argmax(ucb_list)
