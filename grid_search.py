import datetime
import json
import os
import random

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
from scipy.stats import binned_statistic

import configurations as con
import context_partition_node as cpn
import sim_estimated_dist as sed
import sim_metric as sm
import sim_single as snd
import sim_true_distance as std

plt.switch_backend('agg')
sn.set_style("darkgrid")


def get_random_context():
    return random.uniform(0, 1)


def get_avg_cumm_reward(_cum_r_list):
    if len(_cum_r_list) > 0:
        cumm_r_plot = [_cum_r_list[0]]
        cumm_r = _cum_r_list[0]
        for idx, cr in enumerate(_cum_r_list[1:]):
            cumm_r_plot.append((cumm_r + cr) / (idx + 2.0))
            cumm_r = cumm_r + cr
    else:
        return []

    bin_means = binned_statistic(range(0, len(cumm_r_plot)), cumm_r_plot, statistic='mean', bins=1000)[0]
    return bin_means.tolist()


def plot_arm_frequency(inst_algo, file_name, flag, no_arms, no_trils, dname):
    plt.clf()
    fig = plt.figure(figsize=(5, 1.7), dpi=300)
    f = inst_algo.arm_frequency
    # print("Frequency", f)
    e = no_trils / 4
    step = no_trils / 4
    col = 1
    cbar_ax = fig.add_axes([0.6, .4, .3, .03])
    for s in range(0, len(f), step):
        heatmap_data_all = np.zeros((no_arms, 10))
        heatmap_data_explore = np.zeros((no_arms, 10))
        heatmap_data_exploit = np.zeros((no_arms, 10))
        for _tuple in f[s:e]:
            a = _tuple[1]
            cont = int(_tuple[0] / 0.1)
            heatmap_data_all[(a), cont] = heatmap_data_all[a, cont] + 1
            if _tuple[2] == 1:
                heatmap_data_exploit[(a), cont] = heatmap_data_exploit[a, cont] + 1
            if _tuple[2] == 0:
                heatmap_data_explore[(a), cont] = heatmap_data_explore[a, cont] + 1
        e = min(e + step, len(f))
        # print(heatmap_data)
        sb = plt.subplot(1, 4, col)
        if flag == 2:
            sn.heatmap(heatmap_data_all, cbar=s == 0, cbar_ax=None if s else cbar_ax,
                       cbar_kws={"orientation": "horizontal"})
        elif flag == 1:
            sn.heatmap(heatmap_data_exploit, cbar=s == 0, cbar_ax=None if s else cbar_ax,
                       cbar_kws={"orientation": "horizontal"})
        elif flag == 0:
            sn.heatmap(heatmap_data_explore, cbar=s == 0, cbar_ax=None if s else cbar_ax,
                       cbar_kws={"orientation": "horizontal"})
        sb.set_xlabel("t=" + str(col))
        col = col + 1
    fig.text(0.5, 0.0, 'Context Interval', ha='center')
    fig.text(0.0, 0.5, 'Finite Arms', va='center', rotation='vertical')
    # fig.suptitle('Selected Arm Type Frequency_'+str(flag))
    plt.tight_layout()
    plt.savefig(dname + "arm_frequency_" + file_name + "_" + str(flag) + ".pdf")

    # if len(f) > 0:
    #     histogram_data_arms = list(zip(*f)[1])
    #     histogram_data_context = list(zip(*f)[0])
    #     plt.clf()
    #     ax = plt.subplot(2,1,1)
    #     sn.distplot(histogram_data_arms, kde=False, rug=True)
    #     ax.set_title("arm selection distribution")
    #     ax =plt.subplot(2,1,2)
    #     sn.distplot(histogram_data_context, kde=False, rug=True)
    #     ax.set_title("context distribution")
    #     plt.savefig(pngs_dir_name+"total_arm_f_"+file_name+".pdf")


def plot_graphs(algos, no_arms, no_trials, dirname):
    dict_cumm_rewards = {}

    for algo_name in algos.keys():
        plot_arm_frequency(algos[algo_name], algo_name, 2, no_arms, no_trials, dirname)
        if 'our-method' in algo_name:
            plot_arm_frequency(algos[algo_name], algo_name, 1, no_arms, no_trials, dirname)
            plot_arm_frequency(algos[algo_name], algo_name, 0, no_arms, no_trials, dirname)
        dict_cumm_rewards[algo_name] = get_avg_cumm_reward(algos[algo_name].cum_r_list)

    plot_ctr(dict_cumm_rewards, no_trials, no_arms, dirname)

    # if 'single' in algos.keys():
    #     for a in algos["single"].dict_arms.values():
    #         plt.clf()
    #         a.plot()
    #
    #
    #     df = pd.DataFrame({'get_reward': algos["single"].get_reward_elapsed_time,
    #                        'update': algos["single"].update_elapsed_time})
    #
    #     sn.lineplot(data=df)
    #     plt.xlabel("trial")
    #     plt.ylabel("time")
    #     plt.savefig(pngs_dir_name+"elapsed_time_for_GP_single.pdf")

    if 'our-method' in algos.keys():
        plt.clf()
        df_our_method_explore_exploit_trend = pd.DataFrame(algos['our-method'].exploit_or_explore)
        print(df_our_method_explore_exploit_trend.shape)
        print(df_our_method_explore_exploit_trend.head(10))
        print(df_our_method_explore_exploit_trend[0].value_counts())
        sn.lineplot(data=df_our_method_explore_exploit_trend)
        df_our_method_explore_exploit_trend.to_csv(dirname + "exploit_explore_data.csv")
        plt.ylim(-1, 1.5)
        plt.xlim(-1, no_trials + 1)
        plt.xlabel("trials")
        plt.ylabel("is_exploit")
        plt.savefig(dirname + "explore_vs_exploite_trend.pdf")


def plot_ctr(dict_cumm_rewards, trials, no_Arms, dname):
    df_ctr = pd.DataFrame(dict_cumm_rewards)
    df_ctr.to_csv(dname + "ctr_" + str(trials) + ".csv")

    plt.clf()
    plt.figure(figsize=(5, 3), dpi=300)
    df_ctr = pd.DataFrame(dict_cumm_rewards)
    sn.lineplot(data=df_ctr)
    plt.xlabel("No. of trials")
    plt.ylabel("Avg cumm. reward")
    plt.legend(prop={'size': 6})
    plt.tight_layout()
    plt.savefig(dname + 'sim_result_' + str(no_Arms) + '.pdf')


def dump_results(algos, path):
    frequency_list = []
    for algo in algos.keys():
        frequency_list.append({algo: algos[algo].arm_frequency})
    with  open(path + 'frequency.json', 'a') as f:
        json.dump(frequency_list, f)
    f.close()


def dump_algo_configurations(algos, path):
    for algo_name in algos:
        algo = algos[algo_name]
        algo.configs.get_str_configations(algo_name, path)


def run_grid_instance(arm, trial, noise, s_bias, s_dist, cont_disc_n, sampling_constant, neighbours, min_k, s_id,
                      dir_name, algo_dict):
    algo_est_2 = sed.EstDist(2,
                             con.Configs(trial, arm, s_bias, s_dist, sampling_constant, noise, cont_disc_n, neighbours,
                                         min_k))

    for t in range(0, trial):
        print('T === ', t)
        x_t = get_random_context()
        print('x_t ', x_t)

        algo_est_2.get_reward(x_t, None)

    algo_dict["Approx-Zooming"]= algo_est_2

    dir_name = dir_name + s_id + "/"
    os.mkdir(dir_name)

    with open(dir_name + 'partition_distance_comparison_estimated.json', 'a') as f:
        json.dump('phase_index|tilde_dist|true_dist|arm_radius\n', f)
        print(algo_dict["Approx-Zooming"])
        json.dump(algo_dict["Approx-Zooming"].str_partition_distance_comparison_true, f)
    f.close()

    with open(dir_name + 'partition_distance_comparison_true.json', 'a') as f:
        json.dump('phase_index|true_dist|arm_radius\n', f)
        json.dump(algo_dict["Approx-Zooming-True"].str_partition_distance_comparison_true, f)
    f.close()

    with open(dir_name + 'partition_distance_comparison_metric.json', 'a') as f:
        json.dump('phase_index|metric_dist|arm_radius\n', f)
        json.dump(algo_dict["Approx-Zooming-Similarity-Metric"].str_partition_distance_comparison_true, f)
    f.close()


    f = open(dir_name + "partitions.txt", 'w')

    f.write("Partitions with True Distance\n")
    cpn.printTree(algo_dict["Approx-Zooming-True"].root_not_flagged_partitions, f)
    f.write('Partitions with Distance Metric\n')
    cpn.printTree(algo_dict["Approx-Zooming-Similarity-Metric"].root_not_flagged_partitions, f)
    f.write('Partitions with No Distance\n')
    cpn.printTree(algo_dict["Approx-Zooming-No-Arm-Similarity"].root_not_flagged_partitions, f)
    f.write("\nOUR METHOD\n")
    f.write("===NOT-FLAGGED===\n")
    cpn.printTree(algo_est_2.root_not_flagged_partitions, f)
    f.write("===FLAGGED===\n")
    cpn.printTree(algo_est_2.root_flagged_partitions, f)

    f.close()

    plot_graphs(algo_dict, arm, trial, dir_name)

    print(arm, trial, noise)

    dump_results(algo_dict, dir_name)
    dump_algo_configurations(algo_dict, dir_name)


def run_grid_instance_TrueDist_X(arm, trial, noise, s_bias, s_dist, cont_disc_n, algos_dict):
    algo_true_2 = std.TrueDist(2, con.Configs(trial, arm, s_bias, s_dist,
                                              None, noise, cont_disc_n, None, None))
    algo_metric_2 = sm.TrueDistMetric(2, con.Configs(trial, arm, s_bias, s_dist,
                                                     None, noise, cont_disc_n, None, None))


    for t in range(0, trial):
        print('T === ', t)
        x_t = get_random_context()
        print('x_t ', x_t)

        algo_true_2.get_reward(x_t, None)
        algo_metric_2.get_reward(x_t, None)

    algos_dict["Approx-Zooming-True"] = algo_true_2
    algos_dict["Approx-Zooming-Similarity-Metric"] = algo_metric_2

    return algos_dict


def run_grid_instance_Single(arm, trial, noise):  # TODO check changes to boas doesnt change the result

    algo_single_2 = snd.Single(2, con.Configs(trial, arm, 0.25, None,  # tunning bias should not change the result!
                                              None, noise, None, None, None))

    algos = {
        "Approx-Zooming-No-Arm-Similarity": algo_single_2,
    }

    # for avg_count in range(0,5):
    for t in range(0, trial):
        print('T === ', t)
        x_t = get_random_context()
        print('x_t ', x_t)
        algo_single_2.get_reward(x_t, None)

    return algos


def get_grid_instance_id(params_list):
    # return str(params_list).strip('[]')
    return "_".join(map(str, params_list))


if __name__ == '__main__':

    # Common parameters that does not need to be individually tuned
    arms_range = [100, 200, 50]
    trials_range = [10000, 50000, 100000]
    noise_range = [1e-1, 1e-2, 1e-3]

    # grid search parameters #TODO pick more sensible ranges??
    s_bias_range = [0.25, 0.5, 1]
    s_dist_range = [0.25, 0.5, 1]
    sampling_constant_range = [1, 2, 3]
    context_disc_n_range = [5, 6, 7]
    neighbours_range = [5, 10, 15]
    min_k_range = [3, 4, 5]  # there is a relationship with sampling constant and neighbours

    flag_run_single = False
    flag_run_true = False

    folder_path = '/Users/nirandikawanigasekara/PycharmProjects/ApproxZooming/plots/grid_search/'
    folders = ["_".join(name.split('_')[1:]) for name in os.listdir(folder_path) if os.path.isdir(folder_path)]

    print(len(folders))

    for arm in arms_range:
        for trial in trials_range:
            for noise in noise_range:
                if flag_run_single:
                    algos_done = run_grid_instance_Single(arm, trial, noise)
                for s_bias in s_bias_range:
                    for s_dist in s_dist_range:
                        for context_disc_n in context_disc_n_range:
                            if flag_run_true:
                                algos_done = run_grid_instance_TrueDist_X(arm, trial, noise, s_bias, s_dist, context_disc_n, algos_done)
                            for sampling_constant in sampling_constant_range:
                                for neighbours in neighbours_range:
                                    for min_k in min_k_range:
                                        str_id = get_grid_instance_id(
                                            ["a", arm, "t", trial, "n", noise, "b", s_bias, "d", s_dist, "c",
                                             context_disc_n,
                                             "s", sampling_constant, "nei", neighbours, "k", min_k])
                                        if str_id in folders:
                                            print("skipped")
                                        else:
                                            if not flag_run_single:
                                                algos_done = run_grid_instance_Single(arm, trial, noise)
                                            if not flag_run_true:
                                                algos_done = run_grid_instance_TrueDist_X(arm, trial, noise, s_bias,
                                                                                          s_dist, context_disc_n,
                                                                                          algos_done)

                                            flag_run_single = True
                                            flag_run_true = True

                                            dir_name = "plots//grid_search//" + datetime.datetime.now().strftime(
                                                "%Y-%m-%d %H-%M-%S") + "_"
                                            run_grid_instance(arm, trial, noise, s_bias, s_dist, context_disc_n,
                                                              sampling_constant, neighbours, min_k, str_id,
                                                              dir_name, algos_done)

                                        # if arm==100 and trial == 10000 and noise>=0.01 and s_bias < 1 and s_dist< 0.5 and context_disc_n< 6 and sampling_constant <2 and neighbours< 10 and min_k<4:
                                        #     print("skipped")
                                        # else:
                                        #     if arm==100 and trial == 10000 and noise==0.1:
                                        #         print("skipped")
                                        #     else:
                                        #         if arm == 100 and trial == 10000 and noise == 0.01 and s_bias<1:
                                        #             print("skipped")
                                        #         else:
                                        #             if s_dist< 0.5:
                                        #                 print("skipped")
                                        #             else:
                                        #                 if context_disc_n< 6:
                                        #                     print("skipped")
                                        #                 else:
                                        #                     if sampling_constant <2:
                                        #                         print("skipped")
