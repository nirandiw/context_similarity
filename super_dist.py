# import configurations as configs
import numpy as np
import math


def get_lipschitz_reward(context, y_t):
    arm_class = get_arm_class(y_t)
    #abs_diff = abs(context-(arm_class / 10.0))
    abs_diff = abs(context - ((arm_class + 0.5) / 10.0))
    y_t_true_r = np.divide(np.divide(1,abs_diff+1), get_reward_max())
    # y_t_true_r = 1 - (context * arm_class / 10.0)
    return y_t_true_r


def get_function_1_reward(context, y_t, no_A, lips=1):
    y_t = np.divide(y_t, (no_A*1.0))
    y_t_mean = 1-(lips* abs(context-y_t))
    return y_t_mean


def get_function_2_reward(context, y_t, no_A, lips=1):
    y_t = np.divide(y_t, (no_A*1.0))
    y_t_mean_min_0 = abs(y_t - 0)
    y_t_mean_min_0_5 = abs(y_t - 0.5)
    y_t_mean_min_1 = abs(y_t - 1)

    min_array = np.minimum(y_t_mean_min_0, y_t_mean_min_0_5)
    min_array = np.minimum(min_array, y_t_mean_min_1)
    y_t_mean = 1 - (lips * abs(context - (4 * min_array)))
    return y_t_mean


def get_noisy_reward(context, y_t, flag, no_A, noise, lips=1):
    y_t_mean = 0
    if flag == 0:
        y_t_mean = get_lipschitz_reward(context, y_t)
    elif flag == 1:
        y_t_mean = get_function_1_reward(context, y_t, no_A, lips)
    elif flag == 2:
        y_t_mean = get_function_2_reward(context, y_t, no_A, lips)
    # associate y to a latent value with 0-1
    # check the contribution of x and y to the reward
    # For lipschitz keep it linear
    pi_t = np.random.normal(y_t_mean, noise)  # TODO Tune the noise
    return pi_t  # sigma set to 2


def is_within_context_range(_context, context_range):
        if context_range[0] <= _context <= context_range[1]:
            return True
        else:
            return False


def get_distance(c, a, _cxt, flag, _discrete_true_n, no_A):
    data_predict = np.linspace(_cxt[0], _cxt[1], _discrete_true_n)
    if flag == 0:
        f_c = get_lipschitz_reward(data_predict, c)
        f_a = get_lipschitz_reward(data_predict, a)
    elif flag == 1:
        f_c = get_function_1_reward(data_predict, c, no_A)
        f_a = get_function_1_reward(data_predict, a, no_A)
    elif flag == 2:
        f_c = get_function_2_reward(data_predict, c, no_A)
        f_a = get_function_2_reward(data_predict, a, no_A)

    mean_diff = np.subtract(f_c, f_a)
    squared_diff = np.square(mean_diff)
    D_c_a = math.sqrt(np.divide(np.sum(squared_diff), _discrete_true_n))
    #print(" D(c,a)", D_c_a)
    return D_c_a

def get_distance_matric(c, a, _cxt, func, no_A,lips=1):
    dist = 0
    if func ==2: # function 2
        dist = 4*lips* abs(c-a) # abs(np.divide(c, configs.no_A*1.0)-np.divide(a,configs.no_A*1.0))
    elif func == 1: # function 1
        dist = abs(np.divide(c, no_A*1.0)-np.divide(a,no_A*1.0))
    elif func == 0: # finite types
        dist= abs(get_arm_class(c)-get_arm_class(a))
    else:
        print("Invalid reward function")
        exit(-1)
    return dist


def get_rho_true_distance(new_partition, center, flag, discrete_n, no_A):
    true_d = 0
    for a in new_partition.A:
        if a is not center:
            true_d = true_d + get_distance(center, a, new_partition.c, flag, discrete_n, no_A)
    if len(new_partition.A) == 1:
        assert center == new_partition.A[0]
        return 0
    else:
        return true_d / (len(new_partition.A) - 1.0)


def get_relevant_rhos(_x_t, _root_not_flagged_partitions):  # do a binary search and find the partitions
    _partitions = []
    _root_not_flagged_partitions.find_relevant_partitions(_x_t, _partitions)
    #print("Relevant partitions")
    #for p in _partitions:
    #    print("Context "+str(p.c)+" Arms "+str(p.A))
    return _partitions


def get_reward_max(no_A):
    X = np.arange(0, 1, 0.1)
    Y = np.arange(0, no_A, 1)
    X, Y = np.meshgrid(X, Y)
    abs_diff = abs(X - ((Y + 0.5) / 10.0))
    # print(abs_diff)
    Z = np.divide(1, abs_diff + 1)
    max_Z = np.max(Z)
    return max_Z

def get_arm_class(y_t):
    return y_t % 10


def get_rho_metric_distance(new_partition, center, flag, no_A): # this function is wrong
    max_diff = float("-inf")
    # center = new_partition.A[0]
    for a in new_partition.A[1:]:
        dist = get_distance_matric(center, a, center, flag, no_A)
        if max_diff < dist:
            max_diff = dist
    return max_diff


def get_rel_global_samples(global_explore_samples, context, arms):
    rel_global_samples = {}

    for arm_id in arms:
        for point in global_explore_samples[arm_id]:
            if context[0] <= point[0] <= context[1]:
                if arm_id in rel_global_samples.keys():
                    rel_global_samples[arm_id].append(point)
                else:
                    rel_global_samples[arm_id] = [point]
    return rel_global_samples