from __future__ import print_function
import partition as par

class Node:

    def __init__(self, _context_tuple,_partitions):

        self.left = None
        self.right = None
        # self.parent = None
        self.data_rhos = _partitions # list of flagged Partition objects
        self.data_context = _context_tuple
        self.data_context_mid = (self.data_context[1]-self.data_context[0])/ 2.0

    def find_flagged(self, d):
        if self.data_context[0] <= d < self.data_context[1] and len(self.data_rhos) > 0:
            return True, self
        else:
            if d < self.data_context_mid:
                if self.left is not None:
                    return self.left.find_flagged(d)
                else:
                    return False, None
            elif d >= self.data_context_mid:
                if self.right is not None:
                    return self.right.find_flagged(d)
                else:
                    return False, None
            else:
                print("Unhandled scenerio in Tree Find()")
                exit(-1)

    def insertlnOrder(self, cp, n_rhos):
        if cp == self.data_context:
            self.data_rhos.extend(n_rhos)

        elif cp[0] < self.data_context_mid and cp[1] <= self.data_context_mid:
            if self.left is None:
                new_node = Node(cp, n_rhos)
                #new_node.parent = self
                self.left = new_node
            else:
                self.left.insertlnOrder(cp, n_rhos)
        elif cp[0] >= self.data_context_mid and cp[1] > self.data_context_mid:
            if self.right is None:
                new_node = Node(cp, n_rhos)
                self.right = new_node
                #new_node.parent = self
                self.right = new_node
            else:
                self.right.insertlnOrder(cp, n_rhos)

    def remove(self, _rho):
        if _rho.c == self.data_context:
            # print(_rho)
            # print(self.data_rhos)
            self.data_rhos.remove(_rho)
        elif _rho.c[0] < self.data_context_mid and _rho.c[1] <= self.data_context_mid:
            self.left.remove(_rho)
        elif _rho.c[0] >= self.data_context_mid and _rho.c[1] > self.data_context_mid:
            self.right.remove(_rho)

    def find_relevant_partitions(self, d, _partitions):
        if self.data_context[0] <= d < self.data_context[1] and len(self.data_rhos) > 0:
            _partitions.extend(self.data_rhos)
            # print(_partitions)

        if d < self.data_context_mid:
            if self.left is not None:
                self.left.find_relevant_partitions(d, _partitions)
                # print(_partitions)
        elif d >= self.data_context_mid:
            if self.right is not None:
                self.right.find_relevant_partitions(d, _partitions)
                # print(_partitions)
        else:
            print("Unhandled scenerio in Tree Find()")
            exit(-1)
        # print(_partitions)
        return _partitions


def printTree(root, file):
    if root is not None:
        printTree(root.left , file)
        print(root.data_context)
        if file is not None:
            file.write(str(root.data_context)+'\n')
        for rho in root.data_rhos:
            print(rho.A, end='| ')
            if file is not None:
                file.write(str(rho.A)+'|\n')
        print('')
        if file is not None:
            file.write('\n')
        printTree(root.right, file)










