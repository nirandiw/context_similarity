import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import spline
import itertools
from itertools import chain

f = open('./remote_server_results/ctr.txt', 'r')
lines = f.readlines()
print(lines)
split_list =list(chain.from_iterable(el.split(',') for el in lines))
split_list = list(filter(lambda a: a != '\n' and a !='(\'binned reward\'', split_list))
print(split_list)
strip_list = [x.strip(' ]))[\narray([') for x in split_list]
print(strip_list)
print(strip_list.index("Estimated"))
print(strip_list.index("True"))
print(strip_list.index("Single"))
est_list = map(float, strip_list[1:1001])
print(est_list)
true_list = map(float, strip_list[1002:2002])
print(true_list)
single_list = map(float, strip_list[2003:])
print(single_list)

x = np.array(range(0, 1000))
x_smooth = np.linspace(x.min(), x.max(), 1000)
y_algo_est = spline(x, est_list, x_smooth)
y_algo_true = spline(x, true_list, x_smooth)
y_algo_single = spline(x, single_list, x_smooth)

f = open("ctr.txt", 'a')
f.write('Estimated\n')
f.write(y_algo_est)
f.write("True\n")
f.write(y_algo_true)
f.write("Single\n")
#f.write(y_algo_single)
f.close()

plt.figure(figsize=(5, 3), dpi=300)
plt.plot(x_smooth, y_algo_est, label="our-method")
plt.plot(x_smooth, y_algo_true, label="true-reward")
plt.plot(x_smooth, y_algo_single, label="single")
plt.xlabel("No. of trials")
plt.ylabel("Avg cumm. reward")
plt.legend(loc='lower right')
plt.savefig('./remote_server_results/sim_result_200.pdf')
