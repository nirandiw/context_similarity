import super_dist as sd
import partition as par
# import configurations as configs
import context_partition_node as cpn
import random
import numpy as np

class CnxtZooming:

    def __init__(self):
        self.congifs = None
        self.sim_space = sd.get_sim_metric()
        self.ball = par.Partition(None, None, None,None, 1, self.congifs.T)
        self.ball.center = (np.random.uniform(0,1), np.random.uniform(0,1))
        self.active_balls = [self.ball]
        self.arm_frequency = []

    def get_reward(self, x_t):
        relevant = self.get_relevant_balls(x_t, self.active_balls)
        max_b = self.get_max_ball(relevant)
        y_t = random.choice(max_b.A)
        self.arm_frequency.append((x_t, y_t,1))
        pi_t = sd.get_noisy_reward(x_t, y_t)
        max_b.update(x_t, y_t, pi_t)
        if max_b.confidence_radius < max_b.bias:
            new_ball = par.Partition(None, None, None, None, max_b.bias/2, self.congifs.T)
            new_ball.center = (x_t, y_t)
            new_ball.parent = max_b
            self.active_balls.append(new_ball)

    def get_relevant_balls(self, x_t, active_balls):
        relevant_balls = []
        for b_org in active_balls:
            smaller_radius_balls =[]
            for b_comp in active_balls:
                if b_org != b_comp and b_org.bias> b_comp:
                    smaller_radius_balls.append(b_comp)
            
            some_y = np.random.uniform()
        return relevant_balls

    def get_max_ball(self, relevant):
        max_ball = None
        return max_ball