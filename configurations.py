import json

# T = 10000
# no_A = 200
#
# starting_true_bias = 0.25 # Check the starting confidence radius and set accordinly?
# starting_true_distance_threshold = 0.07
#
# starting_est_bias = 0.25
# starting_est_distance_threshold = 0.5
#
# starting_heu_bias = 0.25
# starting_heu_distance_threshold = 0.5
#
# starting_metric_bias = 0.25
# starting_metric_distance_threshold = 10
#
# # starting_sample_threshold = 1250 # not using this anymore
# sampling_constant = 2
#
# noise = 1e-5
#
# discrete_true_n = 6
# discrete_est_n = 6
# gp_context_n = 5

# TODO: How to best tune these parameters
# 1. length_scale for RBF kernel - single method
# 2. length_scale for constant kernel - single method
# 3. alpha value for GaussianProcessRegressor - single method
# 4. bandwidth for NonParamREgression - our method
# 5. Number of discritization points - our method
# 6. All the above variables - our method and true distance

class Configs:

    def __init__(self, T, no_A,
                 # s_true_bias,s_true_dist,
                 s_X_bias, s_X_dist,
                 # s_heu_bias, s_heu_dist,
                 # s_mat_bias, s_mat_dis,
                 sampling_constant,
                 noise, context_X_n,
                 neighbours, min_k):
        self.T = T
        self.no_A = no_A

        self.starting_X_bias = s_X_bias  # Check the starting confidence radius and set accordinly?
        self.starting_X_distance_threshold = s_X_dist

        # self.starting_est_bias = None
        # self.starting_est_distance_threshold = None
        #
        # self.starting_heu_bias = None
        # self.starting_heu_distance_threshold = s_heu_dist
        #
        # self.starting_metric_bias = s_mat_bias
        # self.starting_metric_distance_threshold = s_mat_dis

        # starting_sample_threshold = 1250 # not using this anymore
        self.min_k = min_k
        self.global_sampling_constant= sampling_constant # christina introduced
        self.k_delta_function = None
        self.neighbours = neighbours



        self.noise = noise
        self.context_X_n = context_X_n

    ''' Get a json string of the confgurations'''
    # TODO I'm writing the values here straight away. Clean it
    def get_str_configations(self, name, path):
        dict_config = {'name':name,
            'T':self.T,
            'no_A': self.no_A,
            'start_bias': self.starting_X_bias,
            'start_distance_threhold': self.starting_X_distance_threshold,
            'global_sampling_constant': self.global_sampling_constant,
            'min_k': self.min_k,
            'k_delta_function': self.k_delta_function,
            'noise': self.noise,
            'context_X_n': self.context_X_n,

        }
        with open(path+name+'.json', 'a') as f:
            json.dump(dict_config, f)
        f.close()


# TODO tunnning parameters
#1. initial neighbours
#2. stopping number of neighbours.
# put line numbers to where each of the tunning parameters are
# write up the tunning
