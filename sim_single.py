import random
import numpy as np
import partition as par
import math
# import configurations as configs
import context_partition_node as cpn
import super_dist as sd

np.random.seed(500)


class Single:
    def __init__(self, reward_function, configuration):
        self.configs = configuration
        self.root_not_flagged_partitions = cpn.Node((0.0, 1.0), [])

        for arm in range(0, self.configs.no_A):
            self.root_not_flagged_partitions.insertlnOrder((0.0, 0.5), [
                par.Partition((0.0, 0.5), [arm], None, None, self.configs.starting_X_bias,
                              self.configs.T)])
            self.root_not_flagged_partitions.insertlnOrder((0.5, 1.0), [
                par.Partition((0.5, 1.0), [arm], None, None, self.configs.starting_X_bias,
                              self.configs.T)])
        self.cum_r_list = []
        # self.is_not_init = False
        self.arm_frequency = []
        self.temp_t = 1.0
        self.ctr = 0
        self.cumm_reward = 0
        self.temp_T = 0
        self.reward_function_flag = reward_function

    def find_closest_center(self, centers, a, context_width, new_dist):
        min_dist = float('inf')
        min_c = None
        for c in centers.keys():
            print('c =', c, ' a = ', a)
            true_distance = sd.get_distance(c, a, context_width, self.reward_function_flag)
            if true_distance <= new_dist:
                if true_distance < min_dist:
                    min_dist = true_distance
                    min_c = c
        return min_c

    def do_partiion(self, _rho, path):
        print("Old Trees")
        cpn.printTree(self.root_not_flagged_partitions, None)

        new_context_partitions = [(_rho.c[0], (_rho.c[1] + _rho.c[0]) / 2.0),
                                  ((_rho.c[1] + _rho.c[0]) / 2.0, _rho.c[1])]

        # print("old distance", _rho.distance_threshold)
        # new_eta_2 = np.divide(_rho.distance_threshold, 2)
        # print("new distance ", new_eta_2)

        for cp in new_context_partitions:
            # random.shuffle(_rho.A)
            new_partitions = []
            # centers = {_rho.A[0]: []}

            # for a in _rho.A[1:]:
            #     center = self.find_closest_center(centers, a, cp, new_eta_2)
            #     if center is None:
            #         centers[a] = []
            # for a in _rho.A:
            #     center = self.find_closest_center(centers, a, cp, new_eta_2)
            #     if center is None:
            #         print("Logic error in clustering")
            #         exit(-1)
            #     else:
            #         centers[center].append(a)

            # for center in centers.keys():
                # centers[center].append(center)
            assert len(_rho.A) == 1
            new_partition = par.Partition((cp[0], cp[1]), _rho.A, None, None,
                                              _rho.bias / 2.0, self.configs.T)  # TODO get the correct thresholds and bias?
                #new_partition.set_exploit_samples_from_parent(_rho)
            new_partitions.append(new_partition)

                # rho_true_distance = sd.get_rho_true_distance(new_partition, center, self.reward_function_flag)
                # f = open(path+"partition_distance_comparison_single.txt", 'a')
                # f.write(str(self.temp_T) + '|' + str(rho_true_distance) +'|-\n')
                # f.close()

            self.root_not_flagged_partitions.insertlnOrder(cp, new_partitions)
            # print("New context paritions ", cp)
            # print("root_not_flagged_partitions")
            # cpn.printTree(self.root_not_flagged_partitions)

        # print("root_flagged_partitions")
        # cpn.printTree(root_flagged_partitions)

    def exploit_ucb(self, _x_t, _partitions, path):
        max_rho = _partitions[0]
        max_I_t = _partitions[0].get_I_t()

        for p in _partitions[1:]:
            if p.get_I_t() > max_I_t:
                max_I_t = p.get_I_t()
                max_rho = p

        y_t = max_rho.play_rr_arm() #random.choice(max_rho.A)
        print("Exploit y_t", y_t)
        self.arm_frequency.append((_x_t,y_t,1))
        pi_t = sd.get_noisy_reward(_x_t, y_t, self.reward_function_flag, self.configs.no_A, self.configs.noise)
        max_rho.update(_x_t, y_t, pi_t)

        if max_rho.confidence_radius < max_rho.bias:
            self.do_partiion(max_rho, path)
            self.root_not_flagged_partitions.remove(max_rho)
        return pi_t

    # Give priority to the partition with the longest length (uniform sampling)
    # search the binary and find the first instance with a flagged partition. It is the longest length
    # def is_flagged(self, _x_t, _root_flagged_partitions):
    #     _is_flag, _prior_rel_rhos = _root_flagged_partitions.find_flagged(_x_t)
    #     return _is_flag, _prior_rel_rhos

    def get_reward(self, x_t, path):
        relevant_rhos = sd.get_relevant_rhos(x_t, self.root_not_flagged_partitions)
        r_t = self.exploit_ucb(x_t, relevant_rhos, path)
        print("Reward ", r_t)
        self.cum_r_list.append(r_t)

        self.temp_T = self.temp_T + 1

        print("CTR")
        self.cumm_reward = self.cumm_reward + r_t
        self.ctr = self.cumm_reward / self.temp_t
        self.temp_t = self.temp_t + 1
        print(self.ctr)
