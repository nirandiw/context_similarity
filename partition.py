import math
# import configurations as configs
import itertools
class Partition:

    def __init__(self, (c_0,c_1) ,set_of_arms, sample_threshold, distance_threshold, _bias, _t):
        self.T = _t
        self.A = set_of_arms  # list of arms in the partition or the contnous arm space
        self.c = (c_0,c_1)

        # to implement Slivkins balls
        self.center = None

        self.explore_samples = {} # set of samples in the explore phase M_p {arm:[[context, reward],...]
        self.explore_samples_count = 0 # n_p(t)

        self.cumm_reward = 0.0
        self.exploit_samples = {} # {arm:[[context, reward],...]
        self.exploit_samples_count = 0.0

        self.sample_threshold = sample_threshold
        self.distance_threshold = distance_threshold

        self.bias = _bias
        self.confidence_radius = math.sqrt(math.log(self.T) / (1.0+self.exploit_samples_count))
        self.parent = None

        self.round_robin = itertools.cycle(self.A)

    def get_I_t(self):
        avg_reward = 0 if self.cumm_reward == 0 else (self.cumm_reward / self.exploit_samples_count)
        # print("Arms ", self.A, "Context ", self.c)
        # print("Average reward ", avg_reward, "Cumm reward", self.cumm_reward, "bias", self.bias, "confidence radius", self.confidence_radius)
        I_t = avg_reward + self.bias + self.confidence_radius
        # print("I_t ", I_t)

        if self.parent is not None:
            parent_I_t = self.parent.get_I_t()
        else:
            parent_I_t = I_t

        return min(parent_I_t, I_t)

    def update(self, x_t, y_t, pi_t):
        self.update_exploit_samples(pi_t, x_t, y_t)
        self.cumm_reward = self.cumm_reward + pi_t
        self.exploit_samples_count = self.exploit_samples_count + 1
        print("Current confidence radius", self.confidence_radius)
        self.confidence_radius = math.sqrt(math.log(self.T) / (1.0+self.exploit_samples_count))
        print("Updated confidence radius", self.confidence_radius,
              'bias', self.bias,
              'context', self.c)

    def update_exploit_samples(self, pi_t, x_t, y_t):
        if y_t in self.exploit_samples.keys():
            self.exploit_samples[y_t].append([x_t, pi_t])
        else:
            self.exploit_samples[y_t] = [[x_t, pi_t]]

    def printPartition(self):
        print("Printing Partition Details")
        print(self.A, self.c, self.explore_samples, self.explore_samples_count, self.cumm_reward, self.exploit_samples,
              self.exploit_samples_count, self.sample_threshold, self.distance_threshold, self.bias, self.confidence_radius)

    def set_exploit_samples_from_parent(self, parent):
        assert self.A is not None
        count = 0
        for a in self.A:
            if a in parent.exploit_samples.keys():
                for sample in parent.exploit_samples[a]:
                    if self.c[0] <= sample[0] < self.c[1]:
                        count = count + 1
                        self.update(sample[0], a, sample[1])  # TODO the confidence radius should get updated too?
        print("set_explore_samples_from_parent", str(count))
        self.printPartition()

    def set_parent(self, parent):
        self.parent = parent

    def play_rr_arm(self):
        return self.round_robin.next()



